module.exports = this;

this.data = [
  {
    name: "Ito",
    rating: 4,
    description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).", 
    
    location: {
      address: "Frente al palo e mango al lado la puerta azul.",
      country: "CR",
      city: "San José",
      geo: {
        lat: 9.935697,
        lng: -84.1483647
      }
    },

    totalRooms: 56,
    reservations: 88,
    features: ["wifi", "piscina", "sauna"],

    pictures: [
      'https://www.omnihotels.com/-/media/images/hotels/ausctr/pool/ausctr-omni-austin-hotel-downtown-evening-pool.jpg?h=660&la=en&w=1170'
    ]
  },

  {
    name: "Sote",
    rating: 2,
    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    
    location: {
      address: "Avenida principal # 1",
      country: "CO",
      city: "San Andrés",
      geo: {
        lat: 12.5755662,
        lng: -81.7051197
      }
    },

    totalRooms: 560,
    reservations: 15,
    features: ["tv", "radio", "teléfono"],

    pictures: [
      'http://www.costaadejegranhotel.com/en/img/costa-adeje-gran-hotel-hotel-costa-adeje-piscina-3.jpg', 'http://www.costaadejegranhotel.com/en/img/costa-adeje-gran-hotel-hotel-costa-adeje-piscina-121.jpg'
    ]
  }
]